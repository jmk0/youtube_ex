/** @jsx jsx */
import React from 'react';
import { css, useTheme, jsx } from '@emotion/react';
import { SpinnerCircular } from 'spinners-react';

import Header from './components/Header';
import VideoCard from './components/VideoCard';
// import Spinner from "./components/Spinner";

function App() {
  const theme = useTheme();

  // 반응형 웹 mediaQuery
  // gridLayout
  // IntersectionObserver
  // react suspense
  // error 처리

  return (
    <React.Fragment>
      <Header />
      <main>
        {/* <SpinnerCircular color={theme.mainColor} /> */}
        {/* <React.Suspense fallback={<Spinner />}>
        </React.Suspense> */}
        <div css={gridStyle}>
          {[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map((d) => (
            <VideoCard
              data={{
                thumbnail:
                  'https://i.ytimg.com/vi/u30EfS8_U28/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLCYcZxGWvYX2FRUvrwcC_HTWqDGAQ',
                title:
                  'Playlist ❣️ 떨어진 텐션 멱살 잡고 끌어올려줄 둠칫 노래모음 플레이리스트 광고없음',
                viewCnt: 1400000,
                channelImage:
                  'https://yt3.ggpht.com/oobYTQClSz__kiHijgy06owY_VV36D1O8EFAz1K9_eqRne6HTqNPYr8BCwdYy4BtD706JVH3=s68-c-k-c0x00ffffff-no-rj',
                channelName: '남자사람친구',
                createDate: '2022-01-01',
              }}
            />
          ))}
        </div>
      </main>
    </React.Fragment>
  );
}

const gridStyle = css`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 16px;
  grid-auto-rows: minmax(246px, auto);
  padding: 24px;
`;
export default App;
