import React from 'react';
import { Global, ThemeProvider } from '@emotion/react';
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';

import App from './App';

import GlobalStyle from './styles/reset';
import theme from './styles/theme';

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <Global styles={GlobalStyle} />
      <App />
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);
