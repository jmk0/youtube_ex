import React, { useState } from "react";

function Upload() {
  const [title, setTitle] = useState<string>();
  const [description ,setDescription] = useState<string>();

  return (
    <div>
      <h1>제목</h1>
      <h2>세부정보</h2>
      <div>
        <div>
          <input
            value={title}
            onChange={({ target: { value } }) => setTitle(value)}
          />
          <input
            value={description}
            onChange={({ target: { value } }) => setDescription(value)}
          />
        </div>
        <div>
        <video muted>
          <source src="videos/Clouds.mp4" type="video/mp4" />
          <strong>Your browser does not support the video tag.</strong>
        </video>
        </div>
      </div>
    </div>
  )
}

export default Upload;
