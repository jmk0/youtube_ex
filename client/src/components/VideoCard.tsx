/** @jsx jsx */
import { css, jsx } from '@emotion/react';

type videoItem = {
  thumbnail: string;
  title: string;
  viewCnt: number;
  createDate: string;
  channelImage: string;
  channelName: string;
};

type VideoGridItemProps = {
  data: videoItem;
};

const rounded = css`
  border-radius: 50%;
`;

function VideoCard({
  data: { thumbnail, title, viewCnt, channelImage, channelName, createDate },
}: VideoGridItemProps) {
  return (
    <div css={card}>
      <img src={thumbnail} alt="썸네일" />
      <div css={contentBox}>
        <a href={`/channel/${channelName}`}>
          <img css={avatarStyle} src={channelImage} alt="유튜버 메인 사진" />
        </a>
        <div css={detailBox}>
          <h3>{title}</h3>
          <div css={metaBox}>
            <span>{channelName}</span>
            <span>조회수 {viewCnt}회</span>
            <span>{createDate}</span>
          </div>
        </div>
      </div>
    </div>
  );
}

const card = css`
  overflow: hidden;

  > img {
    width: 100%;
  }

  > div {
    margin-top: 12px;
  }
`;

const contentBox = css`
  display: flex;

  > a {
    margin-right: 12px;
  }
`;

const avatarStyle = css`
  display: inline-block;
  border-radius: 50%;
  width: 36px;
  height: 36px;
`;

const detailBox = css`
  > h3 {
    margin-bottom: 6px;

    font-size: 14px;
    line-height: 1.4;
    display: -webkit-box;
    max-height: ${14 * 1.4 * 2}px;
    -webkit-line-clamp: 2;
    text-overflow: ellipsis;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }
`;

const metaBox = css`
  font-size: 12px;
  color: #606060;

  > span {
    &:first-child {
      display: block;
    }
  }
`;

const detailStyle = css`
  display: flex;

  > h3 {
    margin-bottom: 6px;

    font-size: 1.4rem;
    line-height: 2rem;
    font-weight: 500;
  }
`;

export default VideoCard;
