/** @jsx jsx */
import { useState } from 'react';
import { css, jsx } from '@emotion/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

function SearchForm() {
  const [input, setInput] = useState<string>('');

  function handleChangeInput({ target: { value } }: React.ChangeEvent<HTMLInputElement>) {
    setInput(value);
  }

  function ClearSearhInput() {
    setInput('');
  }

  return (
    <form css={searchFormStyle}>
      <div>
        <input value={input} onChange={handleChangeInput} />
        {input && (
          <button type="button" onClick={ClearSearhInput}>
            <FontAwesomeIcon icon={faXmark} />
          </button>
        )}
      </div>
      <button type="submit">
        <FontAwesomeIcon icon={faMagnifyingGlass} />
      </button>
    </form>
  );
}

const searchFormStyle = css`
  display: flex;
  align-items: center;

  > div {
    display: flex;
    width: 400px;
    height: 40px;
    padding: 2px 6px;
    box-shadow: inset 0 1px 2px #eee;
    border: 1px solid #ccc;
    box-sizing: border-box;

    > input {
      flex-shrink: 1;
      width: 100%;
      height: 100%;
      font-size: 16px;
      line-height: 24px;
    }

    > button {
      flex-shrink: 0;
      width: 40px;
      height: 100%;
    }
  }

  > button {
    border: 1px solid #d3d3d3;
    background-color: #f8f8f8;
    border-radius: 0 2px 2px 0;
    cursor: pointer;
    height: 40px;
    width: 64px;
    margin-left: 4px;
  }
`;

export default SearchForm;
