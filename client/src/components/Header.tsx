/** @jsx jsx */
import { css, jsx } from '@emotion/react';

import SearchForm from '../components/SearchForm';
import Logo from '../components/icon/Logo';

function Header() {
  return (
    <header css={headerStyle}>
      <Logo />
      <SearchForm />
      <div></div>
    </header>
  );
}

const headerStyle = css`
  position: sticky;
  top: 0;
  z-index: 999;
  background: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 56px;
  padding: 0 16px;
  border: 1px solid rgba(0, 0, 0, 0.1);
`;

export default Header;
