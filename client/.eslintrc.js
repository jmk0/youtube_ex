module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "plugin:prettier/recommended",
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "@typescript-eslint"
    ],
    "rules": {
        "printWidth": 100,
        "tabWidth": 2,
        "semi": true,
        "singleQuote": true,
        "quoteProps": "consistent",
        "commaDangle": ["error", "always"],
        "bracketSpacing": true,
        "arrowParens": "always",
    }
}
