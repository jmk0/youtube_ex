import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react({
      jsxRuntime: 'classic',
      babel: {
        plugins: [
          [
            '@emotion',
            {
              autoLabel: 'dev-only',
              labelFormat: '[dirname]-[filename]-[local]',
            },
          ],
          [
            'module-resolver',
            {
              root: ['./src'],
              extensions: ['.ts', '.tsx', '.js', 'jsx', '.json', '.svg'],
            },
          ],
        ],
      },
    }),
  ],
});
