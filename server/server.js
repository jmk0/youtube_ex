var express = require('express');
var path = require('path');

var app = express();

app.use(express.static(path.resolve(__dirname, "../client/dist")));

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../client", "dist", "index.html"));
});

app.listen(5001, function () {
  console.log("start! express server on port 5001")
});
